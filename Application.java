import java.util.Scanner;
public class Application {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		/* Student studentOne = new Student();
		studentOne.name = "Majd";
		System.out.println(studentOne.name);
		studentOne.ID = "9999999";
		System.out.println(studentOne.ID);
		studentOne.grade = 77.9;
		System.out.println(studentOne.grade); */
		
		/* Student studentOne = new Student();
		// System.out.println(studentOne.getName());
		studentOne.setName("Majd");
		System.out.println(studentOne.getName());
		studentOne.setID("9999999");
		System.out.println(studentOne.getID());
		studentOne.setGrade(77.9);
		System.out.println(studentOne.getGrade()); */
		
		// System.out.println("**********");
		
		/* Student studentTwo = new Student();
		studentTwo.setName("Mario");
		System.out.println(studentTwo.getName());
		studentTwo.setID("8888888");
		System.out.println(studentTwo.getID());
		studentTwo.setGrade(99.1);
		System.out.println(studentTwo.getGrade()); */
		
		/* Student[] section3 = new Student[3];
		section3[0] = studentOne;
		section3[1] = studentTwo; */
		
		// System.out.println(section3[0].name);
		// System.out.println(section3[0].ID);
		// System.out.println(section3[0].grade);
		// System.out.println("**********");
		
		/* section3[2] = new Student();
		section3[2].setName("Carol");
		System.out.println(section3[2].getName());
		section3[2].setID("1111111");
		System.out.println(section3[2].getID());
		section3[2].setGrade(88.2);
		System.out.println(section3[2].getGrade()); */
		
		// System.out.println(section3[0].amountLearnt);
		// System.out.println(section3[1].amountLearnt);
		// System.out.println(section3[2].amountLearnt);
		
		/* section3[2].learn();
		section3[2].learn();
		section3[2].learn();
		System.out.println(section3[0].amountLearnt);
		System.out.println(section3[1].amountLearnt);
		System.out.println(section3[2].amountLearnt); */
	
		// System.out.println("value before: " + section3[2].amountLearnt);
		// int amountStudied = reader.nextInt();
		// section3[2].learn(amountStudied);
		// System.out.println("value after: " + section3[2].amountLearnt);
		System.out.println("----------");
		Student aNewStudent = new Student("X", "0000000");
		System.out.println(aNewStudent.getName());
		System.out.println(aNewStudent.getID());
		
		aNewStudent.setGrade(9999.9999);
		System.out.println(aNewStudent.getGrade());
		System.out.println(aNewStudent.getAmountLearnt());
	}
	
}